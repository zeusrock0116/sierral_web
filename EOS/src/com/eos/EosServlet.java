	package com.eos;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Timestamp;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.collections.map.LRUMap;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Servlet implementation class HeartBeatServlet
 */
public class EosServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public static final String ENCODING = "utf8";

	private static LRUMap cache = new LRUMap(10000);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EosServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String proc = request.getRequestURI().substring(
				request.getContextPath().length());
		if (proc.startsWith("/"))
			proc = proc.substring(1);

		response.setContentType("text/plain");
		response.setCharacterEncoding("utf8");

		PrintWriter out = response.getWriter();
	/*	out.println("\nPlease use HTTP POST to call service '" + proc + "'.\n");
		out.println("For example, using Apache HttpClient:\n");

		String s = "\tDefaultHttpClient httpclient = new DefaultHttpClient();\n"
				+ "\tString url = \""
				+ request.getRequestURL()
				+ "\";\n"
				+ "\tHttpPost post = new HttpPost(url);\n"
				+ "\tStringEntity entity = new StringEntity(\"{\\\"mac_id\\\": \\\"20F85EA0B9F4\\\", \\\"state\\\": 1, \\\"dim_value\\\": 80}\", "
				+ "\n\t\tContentType.APPLICATION_JSON);\n"
				+ "\tpost.setEntity(entity);\n"
				+ "\tHttpResponse resp = httpclient.execute(post);\n";
		out.println(s);
		s = "\nOr using curl commandline:\n"
				+ "\tcurl -d '{\"mac_id\": \"20F85EA0B9F4\", \"state\": 1, \"dim_value\": 80}' "
				+ request.getRequestURL()
				+ " --header \"Content-Type:application/json\"";
		out.println(s);
*/
		long used = Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
		String outtext = "\nSystem status:\n" + "\t Used Memory: " + used / 1024 / 1024
				+ " MB\n" + "\t Max  Memory: "
				+ Runtime.getRuntime().maxMemory() / 1024 / 1024 + " MB\n"
				+ "\tMemory Usage: "
				+ (used * 100 / Runtime.getRuntime().maxMemory())
				+ "%\n\tCache Entries: " + cache.size() + "/" + cache.maxSize();
		out.println(outtext);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String ctx = request.getContextPath();
		String uri = request.getRequestURI();
		String proc = "";
		if (ctx.length() + 1 < uri.length())
			proc = request.getRequestURI().substring(
				request.getContextPath().length() + 1);

		response.setContentType("application/json");
		response.setCharacterEncoding("utf8");
		PrintWriter out = response.getWriter();

		JSONParser parser = new JSONParser();
		try {
			JSONObject json = (JSONObject) parser.parse(request.getReader());
			String key = proc + ": " + json.toJSONString();
			
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + " Request:" + key);

			String output = (String) cache.get(key);
			if (output == null) {
				if ("ls_heartbeat".equals(proc)) {
					
					String mac_id = (String) json.get("mac_id");				
					Long state = (Long) json.get("state");
					Long dim_value = (Long) json.get("dim_value");
					Long oPoll = (Long) json.get("poll_interval");
					Long oState = (Long) json.get("istate");
					Long oDimm_value = (Long) json.get("idim_value");
					
					json = ls_heartbeat(mac_id, state, dim_value, oPoll, oState, oDimm_value);
					// Object value = invokeStoredProcedure(proc, json.get("a"),
					// json.get("b"));
					JSONArray array = new JSONArray();
					array.add(json);

					json = new JSONObject();
					json.put("STATE", array);
					output = json.toJSONString();
//					cache.put(key, output);

				}
				else if  ("ls_update".equals(proc)) {
					String mac_id = (String) json.get("mac_id");
					Long state = (Long) json.get("state");
					Long dim_value = (Long) json.get("dim_value");
					Long poll = (Long) json.get("poll_interval");
					Long appchg = (Long) json.get("appchg");
					Long ledonoff = (Long) json.get("ledonoff");
					
					json = ls_setState(mac_id, state, dim_value, poll, appchg, ledonoff);
					
					JSONArray array = new JSONArray();
					array.add(json);

					json = new JSONObject();
					json.put("STATE", array);
					
					output = json.toJSONString();
	//				cache.put(key, output);
		
					
				}
				else if ("ls_getcurrentstate".equals(proc)){
					
					String mac_id = (String) json.get("mac_id");				
					Long oPoll = (Long) json.get("poll_interval");
					Long oState = (Long) json.get("istate");
					Long oDimm_value = (Long) json.get("idim_value");
					
					try{
						json = ls_getCurrentState(mac_id, oPoll, oState, oDimm_value);
						
						if (json == null)
							output = "-1";
						else {
							JSONArray array = new JSONArray();
							array.add(json);
	
							json = new JSONObject();
							json.put("STATE", array);
							output = json.toJSONString();
						}
					}
					catch(SQLException se){
					   //Handle errors for JDBC
					      se.printStackTrace();
					}
					
				}
				else if  ("tt_heartbeat".equals(proc)) {
					String mac_id = (String) json.get("mac_id");				
					Long current_temp = (Long) json.get("current_temp");
					Long setto_temp = (Long) json.get("setto_temp");
					Long status = (Long) json.get("status");
					Long equip_status = (Long) json.get("equip_status");
					String relay_status = (String) json.get("relay_status");	
					
					json = tt_heartbeat(mac_id, current_temp, setto_temp, status, equip_status, relay_status);

					// Object value = invokeStoredProcedure(proc, json.get("a"),
					// json.get("b"));
					JSONArray array = new JSONArray();
					array.add(json);

					json = new JSONObject();
					json.put("STATE", array);
					output = json.toJSONString();
//					cache.put(key, output);
		
					
				}
				
				else if  ("tt_update".equals(proc)) { //for the app
					String mac_id = (String) json.get("mac_id");				
					Long setto_temp = (Long) json.get("setto_temp");
					Long status = (Long) json.get("status");
					Long poll = (Long) json.get("poll_interval");
					
					if (poll < 10000)
						poll = (long) 10000;
									
						
					
					json = tt_update(mac_id, setto_temp, status, poll);

					// Object value = invokeStoredProcedure(proc, json.get("a"),
					// json.get("b"));
					JSONArray array = new JSONArray();
					array.add(json);

					json = new JSONObject();
					json.put("STATE", array);
					output = json.toJSONString();
//					cache.put(key, output);
		
					
				}
				
				else if  ("tt_getconfig".equals(proc)) {
					String mac_id = (String) json.get("mac_id");				
										
					json = tt_getConfig(mac_id);

					// Object value = invokeStoredProcedure(proc, json.get("a"),
					// json.get("b"));
					JSONArray array = new JSONArray();
					array.add(json);

					json = new JSONObject();
					json.put("CONFIG", array);
					output = json.toJSONString();
//					cache.put(key, output);					
				}
				
				else if  ("tt_setconfig".equals(proc)) {
					String mac_id = (String) json.get("mac_id");				
					Long equip_type = (Long) json.get("equip_type");
					Long fanhv_value = (Long) json.get("fanhv_value");
					Long short_cycle = (Long) json.get("short_cycle");
					Long heat_type = (Long) json.get("heat_type");	
					Long cool_type = (Long) json.get("cool_type");	
					String tempunit = (String) json.get("tempunit");
					Long coollimit = (Long) json.get("coollimit");	
					Long heatlimit = (Long) json.get("heatlimit");	
					Long tempdiff = (Long) json.get("tempdiff");	
					
					if (heatlimit < coollimit){
						output = "-1"; } //error
					else{
						json = tt_setConfig(mac_id, equip_type, fanhv_value, short_cycle, heat_type, cool_type, tempunit, coollimit, heatlimit, tempdiff);

						// Object value = invokeStoredProcedure(proc, json.get("a"),
						// json.get("b"));
						JSONArray array = new JSONArray();
						array.add(json);

						json = new JSONObject();
						json.put("CONFIG", array);
						output = json.toJSONString();
//						cache.put(key, output);		
	
					}					
				}
				
				else if  ("tt_getprogram".equals(proc)) {
					String mac_id = (String) json.get("mac_id");				
								
					json = tt_getProgram(mac_id);

					// Object value = invokeStoredProcedure(proc, json.get("a"),
					// json.get("b"));
					JSONArray array = new JSONArray();
					array.add(json);

					json = new JSONObject();
					json.put("PROGRAM", array);
					output = json.toJSONString();
//					cache.put(key, output);
						
				}
				
				else if  ("tt_setprogram".equals(proc)) {
					String mac_id = (String) json.get("mac_id");	
					String program = (String) json.get("program");
								
					json = tt_setProgram(mac_id, program);

					// Object value = invokeStoredProcedure(proc, json.get("a"),
					// json.get("b"));
					JSONArray array = new JSONArray();
					array.add(json);

					json = new JSONObject();
					json.put("PROGRAM", array);
					output = json.toJSONString();
//					cache.put(key, output);
						
				}
				
			}
			
			date = new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + " Response:" + output);
			out.println(output);
			
		} catch (Exception e) {
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + " -1");
			//e.printStackTrace(out);
		}
	}

	@SuppressWarnings("unchecked")
	private JSONObject ls_heartbeat(String mac_id, Long iState, Long iDimm, Long poll, Long state, Long dim_value)
			throws SQLException {

		Connection conn = getConnection();
		try {

			String sql = " {call sp_ls_heartbeat(?, ?, ?, ?, ?, ?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + mac_id + ": "+ iState + ": " + iDimm);
			CallableStatement call = conn.prepareCall(sql);
			try {
				call.setObject(1, mac_id);
				call.setObject(2, iState);
				call.setObject(3, iDimm);
				call.registerOutParameter(4, Types.INTEGER);
				call.registerOutParameter(5, Types.INTEGER);
				call.registerOutParameter(6, Types.INTEGER);
				call.registerOutParameter(7, Types.INTEGER);
				
				
				call.execute();

				JSONObject json = new JSONObject();
				
				json.put("MACID", mac_id);
				json.put("POLLINTERVAL", call.getObject(4));
				json.put("ONOFF", call.getObject(5));
				json.put("DIMVALUE", call.getObject(6));
				json.put("LEDSTATE", call.getObject(7));
				
				return json;
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}
	
	@SuppressWarnings("unchecked")
	private JSONObject ls_getCurrentState(String mac_id, Long poll, Long state, Long dim_value)
			throws SQLException {

		Connection conn = getConnection();
		try {

			String sql = " {call sp_ls_getCurrentState(?, ?, ?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + mac_id);
			CallableStatement call = conn.prepareCall(sql);
			try {
				call.setObject(1, mac_id);
				call.registerOutParameter(2, Types.INTEGER);
				call.registerOutParameter(3, Types.INTEGER);
				call.registerOutParameter(4, Types.INTEGER);
				call.execute();

				JSONObject json = new JSONObject();
				
				if (call.getObject(3)!= null){
					json.put("MACID", mac_id);
					json.put("POLLINTERVAL", call.getObject(2));
					json.put("ONOFF", call.getObject(3));
					json.put("DIMVALUE", call.getObject(4));
				}
				else 
				{
					json = null;
				}
				
					
					
				return json;
			} 
			
			finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}
	
	@SuppressWarnings("unchecked")
	private JSONObject ls_setState(String mac_id, Long iState, Long iDimm, Long iPoll, Long iAppChg, Long iLedonoff)
			throws SQLException {

		Connection conn = getConnection();
		try {

			String sql = " {call sp_ls_updateState(?, ?, ?, ?, ?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + mac_id + ": "+ iState + ": " + iDimm + ": " + iPoll + ": " + iAppChg + ": " + iLedonoff);
			CallableStatement call = conn.prepareCall(sql);
			try {
				call.setObject(1, mac_id);
				call.setObject(2, iState);
				call.setObject(3, iDimm);
				call.setObject(4, iPoll);
				call.setObject(5, iAppChg);
				call.setObject(6,  iLedonoff);
				call.execute();
				
				JSONObject json = new JSONObject();
				json.put("MACID", mac_id);
				json.put("POLLINTERVAL", iPoll);
				json.put("ONOFF", iState);
				json.put("DIMVALUE", iDimm);
				json.put("LEDONOFF", iLedonoff);
				return json;
				
				
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}
	
	@SuppressWarnings("unchecked")
	private JSONObject tt_heartbeat(String imac_id, Long icurrent_temp, Long isetto_temp, Long istatus, Long iequip_status, String irelay_status)
			throws SQLException {

		Connection conn = getConnection();
		try {

			String sql = " {call sp_tt_heartbeat(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + imac_id + ": "+ icurrent_temp + ": " + isetto_temp + ": " + istatus + ": " + iequip_status + ": " + irelay_status);
			CallableStatement call = conn.prepareCall(sql);
			try {
				call.setObject(1, imac_id);
				call.setObject(2, icurrent_temp);
				call.setObject(3, isetto_temp);
				call.setObject(4, istatus);
				call.setObject(5, iequip_status);
				call.setObject(6, irelay_status);
					
				call.registerOutParameter(7, Types.VARCHAR); //date time
				call.registerOutParameter(8, Types.INTEGER); //poll interval
				call.registerOutParameter(9, Types.INTEGER); //setto_temp
				call.registerOutParameter(10, Types.INTEGER); //mode
				call.registerOutParameter(11, Types.INTEGER); //prog_chg
				call.registerOutParameter(12, Types.INTEGER); //config_chg
				call.registerOutParameter(13, Types.INTEGER); //svr_status
				call.registerOutParameter(14, Types.INTEGER); //weekday #
				
				call.execute();

				JSONObject json = new JSONObject();
				
				json.put("MACID", imac_id);
				json.put("TIMESTAMP", call.getObject(7));
				json.put("POLLINTERVAL", call.getObject(8));
				json.put("SETTO_TEMP", call.getObject(9));
				json.put("MODE", call.getObject(10));
				json.put("PROG_CHG", call.getObject(11));
				json.put("CONFIG_CHG", call.getObject(12));
				json.put("STATUS", call.getObject(13));
				json.put("WEEKDAY", call.getObject(14));
				
				return json;
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}
	
	@SuppressWarnings("unchecked")
	private JSONObject tt_update(String imac_id, Long isetto_temp, Long istatus, Long ipoll)
			throws SQLException {

		Connection conn = getConnection();
		try {

			String sql = " {call sp_tt_update(?, ?, ?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + imac_id + ": " + isetto_temp + ": " + istatus + ": " + ipoll);
			CallableStatement call = conn.prepareCall(sql);
			try {
				call.setObject(1, imac_id);
				call.setObject(2, isetto_temp);
				call.setObject(3, istatus);
				call.setObject(4, ipoll);
				call.execute();
				
				JSONObject json = new JSONObject();
				json.put("MACID", imac_id);
				json.put("POLLINTERVAL", ipoll);
				json.put("SETTEMP", isetto_temp);
				json.put("STATUS", istatus);
				return json;
				
				
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}

	@SuppressWarnings("unchecked")
	private JSONObject tt_getConfig(String imac_id)
			throws SQLException {

		Connection conn = getConnection();
		try {

			String sql = " {call sp_tt_getConfig(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + imac_id);
			CallableStatement call = conn.prepareCall(sql);
			try {
				
				call.setObject(1, imac_id);
				
				call.registerOutParameter(2, Types.INTEGER);  //equip_type
				call.registerOutParameter(3, Types.INTEGER);  //fanhv
				call.registerOutParameter(4, Types.INTEGER);  //short cycle
				call.registerOutParameter(5, Types.INTEGER);  //heat_type
				call.registerOutParameter(6, Types.INTEGER);  //cool_type
				call.registerOutParameter(7, Types.VARCHAR);  //tempunit
				call.registerOutParameter(8, Types.INTEGER);  //coollimit
				call.registerOutParameter(9, Types.INTEGER);  //heatlimit
				call.registerOutParameter(10, Types.INTEGER); //tempdiff
								
				call.execute();

				JSONObject json  = new JSONObject();
				
				json.put("MACID", imac_id);
				json.put("EQUIPMENTTYPE", call.getObject(2));
				json.put("FANHV", call.getObject(3));
				json.put("SHORTCYCLE", call.getObject(4));
				json.put("HEATTYPE", call.getObject(5));
				json.put("COOLTYPE", call.getObject(6));
				json.put("TEMPUNIT", call.getObject(7));
				json.put("SPMIN", call.getObject(8));
				json.put("SPMAX", call.getObject(9));
				json.put("TEMPDIFF", call.getObject(10));
				
				return json;
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}
	
	@SuppressWarnings("unchecked")
	private JSONObject tt_setConfig(String imac_id, Long iequip_type, Long ifanhv_value, Long ishort_cycle, Long iheat_type, Long icool_type, String itempunit, Long icoollimit, Long iheatlimit, Long itempdiff)
			throws SQLException {
		
		Connection conn = getConnection();
		try {

			String sql = " {call sp_tt_setConfig(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + imac_id);
			CallableStatement call = conn.prepareCall(sql);
			try {
				
				call.setObject(1, imac_id);
				call.setObject(2, iequip_type);
				call.setObject(3, ifanhv_value);
				call.setObject(4, ishort_cycle);
				call.setObject(5, iheat_type);
				call.setObject(6, icool_type);
				call.setObject(7, itempunit);
				call.setObject(8, icoollimit);
				call.setObject(9, iheatlimit);
				call.setObject(10, itempdiff);
			
				call.registerOutParameter(11, Types.INTEGER); //result
								
				call.execute();

				JSONObject json = new JSONObject();
				
				
				json.put("MACID", imac_id);
				
				if (call.getObject(11).equals(null)) //temporary for testing....for some reason it is always returning null even though stroed proc is correct
					json.put("RESULT", 1);
				else
					json.put("RESULT", call.getObject(11));
				
				return json;
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}

	@SuppressWarnings("unchecked")
	private JSONObject tt_getProgram(String imac_id)
			throws SQLException {
		
		Connection conn = getConnection();
		try {

			String sql = " {call sp_tt_getProgram(?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + imac_id);
			CallableStatement call = conn.prepareCall(sql);
			try {
				
				call.setObject(1, imac_id);
				
				call.registerOutParameter(2, Types.VARCHAR); //result
								
				call.execute();

				JSONObject json = new JSONObject();
				
				json.put("MACID", imac_id);
				json.put("VALUES", call.getObject(2));
				
				return json;
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}
	
	private JSONObject tt_setProgram(String imac_id, String iprogram)
			throws SQLException {
		
		Connection conn = getConnection();
		try {

			String sql = " {call sp_tt_setProgram(?, ?, ?) }";
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql + ": " + imac_id);
			CallableStatement call = conn.prepareCall(sql);
			try {
				
				call.setObject(1, imac_id);
				call.setObject(2, iprogram);
				
				call.registerOutParameter(3, Types.INTEGER); //result
								
				call.execute();

				JSONObject json = new JSONObject();
								
				json.put("MACID", imac_id);
				
				if (call.getObject(3).equals(null)) //temporary for testing....for some reason it is always returning null even though stroed proc is correct
					json.put("RESULT", 1);
				else
					json.put("RESULT", call.getObject(11));
				
				return json;
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}

	}

	
	private Object invokeStoredProcedure(String name, Object... args)
			throws SQLException {
		Connection conn = getConnection();
		try {
			String s = "?";
			for (int i = 0; i < args.length; i++)
				s += ",?";

			String sql = " {call " + name + "(" + s + ") }";
			
			java.util.Date date= new java.util.Date();   
			System.out.println(new Timestamp(date.getTime()) + sql);
			
			CallableStatement call = conn.prepareCall(sql);
			try {
				call.registerOutParameter(1, Types.INTEGER);
				for (int i = 0; i < args.length; i++)
					call.setObject(i + 2, args[i]);
				call.execute();

				return call.getObject(1);
			} finally {
				call.close();
			}
		} finally {
			conn.close();
		}
	}

	private static DataSource dataSource;

	private static Connection getConnection() throws SQLException {
		if (dataSource == null) {
			try {
				InitialContext ctx = new InitialContext();
				dataSource = (DataSource) ctx
						.lookup("java:comp/env/jdbc/heartbeat");
			} catch (NamingException e) {
				try {
					Class.forName("com.mysql.jdbc.Driver").newInstance();
					return DriverManager.getConnection("jdbc:mysql://localhost:3306/sierra45_eos?noAccessToProcedureBodies=true", "sierra45", "S3cur3p@ssw0rd!");
//					return DriverManager.getConnection("jdbc:mysql://localhost:3306/sierral45?noAccessToProcedureBodies=true", "root","");//"sierra45", "S3cur3p@ssw0rd!");
				} catch (Exception e1) {
					throw new SQLException("connecting sierra45_eos", e1);
				}
				
			}
		}
		return dataSource.getConnection();
	}

}
