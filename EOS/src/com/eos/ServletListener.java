package com.eos;

import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

public class ServletListener implements ServletContextListener {

	private static DataSource dataSource;

	private static Connection getConnection() throws SQLException {
		if (dataSource == null) {
			try {
				InitialContext ctx = new InitialContext();
				dataSource = (DataSource) ctx
						.lookup("java:comp/env/jdbc/heartbeat");
			} catch (NamingException e) {
				try {
					Class.forName("com.mysql.jdbc.Driver").newInstance();
					return DriverManager.getConnection("jdbc:mysql://localhost:3306/sierra45_eos?noAccessToProcedureBodies=true", "sierra45", "S3cur3p@ssw0rd!");
//					return DriverManager.getConnection("jdbc:mysql://localhost:3306/sierral45?noAccessToProcedureBodies=true", "root","");//"sierra45", "S3cur3p@ssw0rd!");
				} catch (Exception e1) {
					throw new SQLException("connecting sierra45_eos", e1);
				}
				
			}
		}
		return dataSource.getConnection();
	}
	
	
	
	
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		ServletContext servletContext = arg0.getServletContext();
		Timer timer = (Timer)servletContext.getAttribute ("timer");
		if (timer != null)
		timer.cancel();
		servletContext.removeAttribute ("timer");
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0){
		ServletContext servletContext = arg0.getServletContext();
		int delay = 1000;
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask(){
		public void run(){
			Connection conn;
			try {
				conn = getConnection();
				try{
					//regarding to server timezone.. now server timezone is set to utc+8; so minuse 8 from server timeto get UTC 0;
					
//					int time_differ = -8;
					Calendar utc_timezone = Calendar.getInstance();	
					System.out.println(utc_timezone.getTime().toString());
//					utc_timezone.add(Calendar.HOUR_OF_DAY, time_differ);
					int hour = utc_timezone.get(Calendar.HOUR_OF_DAY);
					if(hour == 0){hour = 12;}
					int minute = utc_timezone.get(Calendar.MINUTE);
					String am_pm = utc_timezone.get(Calendar.AM_PM) == Calendar.AM?"AM":"PM";
					String search_time = String.format("%02d:%02d:%s", hour, minute, am_pm);
					int day_of_week = utc_timezone.get(Calendar.DAY_OF_WEEK);
					day_of_week -= 1;
					String sql = "{call sp_ls_update_schedule(?, ?)}";
					CallableStatement call = conn.prepareCall(sql);
					call.setObject(1, day_of_week);
					call.setObject(2, search_time);
					call.execute();
				}
				finally{
					conn.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		},delay, 60000);
		servletContext.setAttribute ("timer", timer);
	}

}
